package factory;

import de.fatox.rox.shared.util.OxSortDirection;
import org.meanbean.factories.basic.RandomFactoryBase;
import org.meanbean.util.RandomValueGenerator;

/**
 * @author pinkie.swirl@mailbox.org
 */
class OxSortDirectionFactory extends RandomFactoryBase<OxSortDirection>
{

    /**
     * Unique version ID of this Serializable class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new Byte object factory.
     *
     * @param randomValueGenerator A random value generator used by the Factory to generate random values.
     * @throws IllegalArgumentException If the specified randomValueGenerator is deemed illegal. For example, if it is null.
     */
    public OxSortDirectionFactory(RandomValueGenerator randomValueGenerator) throws IllegalArgumentException
    {
        super(randomValueGenerator);
    }

    /**
     * Create a new Byte object.
     *
     * @return A new Byte object.
     */
    @Override
    public OxSortDirection create()
    {
        return new OxSortDirection(getSortOrder());
    }

    private String getSortOrder()
    {
        if (getRandomValueGenerator().nextBoolean())
            return "ASC";
        return "DESC";
    }
}
