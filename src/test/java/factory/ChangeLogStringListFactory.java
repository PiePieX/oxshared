package factory;

import de.fatox.rox.shared.util.ChangelogStringList;
import org.meanbean.factories.basic.RandomFactoryBase;
import org.meanbean.util.RandomValueGenerator;

import java.util.LinkedList;

/**
 * @author pinkie.swirl@mailbox.org
 */
class ChangeLogStringListFactory extends RandomFactoryBase<ChangelogStringList>
{

    /**
     * Unique version ID of this Serializable class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new Byte object factory.
     *
     * @param randomValueGenerator A random value generator used by the Factory to generate random values.
     * @throws IllegalArgumentException If the specified randomValueGenerator is deemed illegal. For example, if it is null.
     */
    public ChangeLogStringListFactory(RandomValueGenerator randomValueGenerator) throws IllegalArgumentException
    {
        super(randomValueGenerator);
    }

    /**
     * Create a new LocalDateTime object.
     *
     * @return A new LocalDateTime object.
     */
    @Override
    public ChangelogStringList create()
    {
        return new ChangelogStringList(new LinkedList<>());
    }

}
