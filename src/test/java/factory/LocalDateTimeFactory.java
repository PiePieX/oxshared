package factory;

import org.meanbean.factories.basic.RandomFactoryBase;
import org.meanbean.util.RandomValueGenerator;

import java.time.LocalDateTime;

/**
 * @author pinkie.swirl@mailbox.org
 */
class LocalDateTimeFactory extends RandomFactoryBase<LocalDateTime>
{

    /**
     * Unique version ID of this Serializable class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new Byte object factory.
     *
     * @param randomValueGenerator A random value generator used by the Factory to generate random values.
     * @throws IllegalArgumentException If the specified randomValueGenerator is deemed illegal. For example, if it is null.
     */
    public LocalDateTimeFactory(RandomValueGenerator randomValueGenerator) throws IllegalArgumentException
    {
        super(randomValueGenerator);
    }

    /**
     * Create a new LocalDateTime object.
     *
     * @return A new LocalDateTime object.
     */
    @Override
    public LocalDateTime create()
    {
        return LocalDateTime.of(
                unsignedInt(999999999) + 1,
                unsignedInt(12) + 1,
                unsignedInt(28) + 1,
                unsignedInt(23),
                unsignedInt(59),
                unsignedInt(59),
                unsignedInt(999999999));
    }

    private int unsignedInt(int max)
    {
        return (Math.abs(getRandomValueGenerator().nextInt()) % max);
    }
}
