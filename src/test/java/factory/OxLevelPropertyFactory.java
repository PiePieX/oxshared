package factory;

import de.fatox.rox.shared.util.OxLevelProperty;
import org.meanbean.factories.basic.RandomFactoryBase;
import org.meanbean.util.RandomValueGenerator;

/**
 * @author pinkie.swirl@mailbox.org
 */
class OxLevelPropertyFactory extends RandomFactoryBase<OxLevelProperty>
{

    /**
     * Unique version ID of this Serializable class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new Byte object factory.
     *
     * @param randomValueGenerator A random value generator used by the Factory to generate random values.
     * @throws IllegalArgumentException If the specified randomValueGenerator is deemed illegal. For example, if it is null.
     */
    public OxLevelPropertyFactory(RandomValueGenerator randomValueGenerator) throws IllegalArgumentException
    {
        super(randomValueGenerator);
    }

    /**
     * Create a new Byte object.
     *
     * @return A new Byte object.
     */
    @Override
    public OxLevelProperty create()
    {
        return new OxLevelProperty(getLevelProperty());
    }

    private String getLevelProperty()
    {
        switch (getRandomValueGenerator().nextInt() % 4)
        {
        case 0:
            return "author.name";
        case 1:
            return "latestUpdateDate";
        case 2:
            return "version";
        case 3:
            return "name";
        default:
            return "";
        }
    }
}
