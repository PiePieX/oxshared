package factory;

import org.meanbean.factories.basic.RandomFactoryBase;
import org.meanbean.util.RandomValueGenerator;

/**
 * @author pinkie.swirl@mailbox.org
 */
class ByteArrayFactory extends RandomFactoryBase<byte[]>
{

    /**
     * Unique version ID of this Serializable class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new Byte object factory.
     *
     * @param randomValueGenerator A random value generator used by the Factory to generate random values.
     * @throws IllegalArgumentException If the specified randomValueGenerator is deemed illegal. For example, if it is null.
     */
    public ByteArrayFactory(RandomValueGenerator randomValueGenerator) throws IllegalArgumentException
    {
        super(randomValueGenerator);
    }

    /**
     * Create a new byte array object.
     *
     * @return A new byte array object.
     */
    @Override
    public byte[] create()
    {
        return getRandomValueGenerator().nextBytes(Math.abs(getRandomValueGenerator().nextInt()) % 100);
    }
}
