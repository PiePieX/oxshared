package factory;

import de.fatox.rox.shared.util.*;
import org.meanbean.factories.FactoryCollection;
import org.meanbean.util.RandomValueGenerator;
import org.meanbean.util.SimpleRandomValueGenerator;

import java.time.LocalDateTime;

/**
 * @author pinkie.swirl@mailbox.org
 */
public enum FactoryHelper
{
    ;

    public static void addFactories(FactoryCollection factoryCollection)
    {
        RandomValueGenerator generator = new SimpleRandomValueGenerator();

        factoryCollection.addFactory(byte[].class, new ByteArrayFactory(generator));
        factoryCollection.addFactory(ChangelogStringList.class, new ChangeLogStringListFactory(generator));
        factoryCollection.addFactory(LocalDateTime.class, new LocalDateTimeFactory(generator));
        factoryCollection.addFactory(OxLevelProperty.class, new OxLevelPropertyFactory(generator));
        factoryCollection.addFactory(OxName.class, new OxNameFactory(generator));
        factoryCollection.addFactory(OxPassword.class, new OxPasswordFactory(generator));
        factoryCollection.addFactory(OxSortDirection.class, new OxSortDirectionFactory(generator));
        factoryCollection.addFactory(OxVersion.class, new VersionFactory(generator));
    }
}
