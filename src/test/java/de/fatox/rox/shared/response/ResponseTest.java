package de.fatox.rox.shared.response;

import org.junit.Before;
import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.meanbean.test.EqualsMethodTester;
import org.meanbean.test.HashCodeMethodTester;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.reflect.Constructor;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static factory.FactoryHelper.addFactories;
import static org.junit.Assert.assertEquals;

/**
 * @author pinkie.swirl@mailbox.org
 */
public class ResponseTest
{
    private BeanTester           beanTester;
    private EqualsMethodTester   equalsMethodTester;
    private HashCodeMethodTester hashCodeMethodTester;
    private Set<Class<?>>        allClassesInPackage;

    @Before
    public void setUp() throws Exception
    {
        beanTester = new BeanTester();
        addFactories(beanTester.getFactoryCollection());

        equalsMethodTester = new EqualsMethodTester();
        addFactories(equalsMethodTester.getFactoryCollection());

        hashCodeMethodTester = new HashCodeMethodTester();
        addFactories(hashCodeMethodTester.getFactoryCollection());

        String prefix = "de.fatox.rox.shared.response";

        List<ClassLoader> classLoadersList = new LinkedList<>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());

        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(prefix))));

        allClassesInPackage = reflections.getSubTypesOf(Object.class);
    }

    @Test
    public void testBean() throws Exception
    {
        for (Class<?> packageClass : allClassesInPackage)
        {
            if (packageClass == this.getClass())
                continue;

            beanTester.testBean(packageClass);
        }
    }

    @Test
    public void testEquals() throws Exception
    {
        for (Class<?> packageClass : allClassesInPackage)
        {
            if (packageClass == this.getClass())
                continue;

            equalsMethodTester.testEqualsMethod(packageClass);
        }
    }

    @Test
    public void testHashCode() throws Exception
    {
        for (Class<?> packageClass : allClassesInPackage)
        {
            if (packageClass == this.getClass())
                continue;

            hashCodeMethodTester.testHashCodeMethod(packageClass);
        }
    }

    @SuppressWarnings("Duplicates")
    @Test
    public void testConstructor() throws Exception
    {
        for (Class<?> packageClass : allClassesInPackage)
        {
            if (packageClass == this.getClass())
                continue;


            for (Constructor<?> constructor : packageClass.getConstructors())
            {
                Object[] params = new Object[constructor.getParameterCount()];
                int      i      = 0;
                for (Class<?> parameterType : constructor.getParameterTypes())
                {
                    params[i++] = beanTester.getFactoryCollection().getFactory(parameterType).create();
                }
                Object o = constructor.newInstance(params);

                assertEquals(packageClass, o.getClass());


            }
        }
    }

    @Test
    public void isRequestCompletedTest() throws Exception
    {
        for (Class<?> packageClass : allClassesInPackage)
        {
            if (packageClass == this.getClass())
                continue;

            for (Constructor<?> constructor : packageClass.getConstructors())
            {
                for (int tests = 0; tests < 100; tests++)
                {
                    Object[] params = new Object[constructor.getParameterCount()];
                    int      i      = 0;
                    for (Class<?> parameterType : constructor.getParameterTypes())
                    {
                        params[i++] = beanTester.getFactoryCollection().getFactory(parameterType).create();
                    }
                    Object o = constructor.newInstance(params);

                    assertEquals(o.getClass(), packageClass);

                    if (params.length == 1 && Set.class.isAssignableFrom(params[0].getClass()) && ((Set) params[0]).size() > 0)
                    {
                        assertEquals(false, ((AbstractResponse) o).isRequestCompleted());
                    }
                    else
                    {
                        assertEquals(true, ((AbstractResponse) o).isRequestCompleted());
                    }
                }
            }
        }
    }
}