package de.fatox.rox.shared.response;


import de.fatox.rox.shared.util.OxVersion;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Set;

/**
 * REST response for uploading a new application version.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@SuppressWarnings("unused")
public class AppUploadVersionResponse extends AbstractResponse
{
    @NonNull
    @NotNull
    @Valid
    private OxVersion version;

    /**
     * Constructs a response with the given error set.
     *
     * @param errors set of error strings
     */
    public AppUploadVersionResponse(Set<String> errors)
    {
        super(errors);
        this.version = new OxVersion();
    }

    public AppUploadVersionResponse(OxVersion version)
    {
        super(Collections.emptySet());
        this.version = version;
    }
}
