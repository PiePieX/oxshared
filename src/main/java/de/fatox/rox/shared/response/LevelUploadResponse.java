package de.fatox.rox.shared.response;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Set;

/**
 * REST response for level upload.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@SuppressWarnings("unused")
public class LevelUploadResponse extends AbstractResponse
{
    @NonNull
    @NotNull
    private Long id;

    /**
     * Constructs a response with the given error set.
     *
     * @param errors set of error strings
     */
    public LevelUploadResponse(Set<String> errors)
    {
        super(errors);
        this.id = -1L;
    }

    public LevelUploadResponse(Long id)
    {
        super(Collections.emptySet());
        this.id = id;
    }
}
