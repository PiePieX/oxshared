package de.fatox.rox.shared.response;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Abstract REST response.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@SuppressWarnings("unused")
public class AbstractResponse
{
    @NonNull
    @NotNull
    private Set<String> errors;

    public boolean isRequestCompleted()
    {
        return errors.isEmpty();
    }
}
