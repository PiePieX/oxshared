package de.fatox.rox.shared.response;


import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Set;

/**
 * REST response for level download.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@SuppressWarnings("unused")
public class LevelDownloadResponse extends AbstractResponse
{
    @NonNull
    @NotNull
    @Valid
    private String data;

    /**
     * Constructs a response with the given error set.
     *
     * @param errors set of error strings
     */
    public LevelDownloadResponse(Set<String> errors)
    {
        super(errors);
        this.data = "INVALID_DATA";
    }

    public LevelDownloadResponse(String data)
    {
        super(Collections.emptySet());
        this.data = data;
    }
}
