package de.fatox.rox.shared.response;

import de.fatox.rox.shared.util.OxName;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Set;

/**
 * REST response for user registration.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@SuppressWarnings("unused")
public class OxUserRegisterResponse extends AbstractResponse
{
    @NonNull
    @NotNull
    @Valid
    private OxName name;

    /**
     * Constructs a response with the given error set.
     *
     * @param errors set of error strings
     */
    public OxUserRegisterResponse(Set<String> errors)
    {
        super(errors);
        this.name = new OxName("INVALID_DATA");
    }

    public OxUserRegisterResponse(OxName name)
    {
        super(Collections.emptySet());
        this.name = name;
    }
}
