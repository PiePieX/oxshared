package de.fatox.rox.shared.response;


import de.fatox.rox.shared.util.LevelView;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * REST response for lvl page view.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@SuppressWarnings("unused")
public class LevelViewPageResponse extends AbstractResponse
{
    @NonNull
    @NotNull
    private Integer         totalPages;
    @NonNull
    @NotNull
    private Integer         number;
    @NonNull
    @NotNull
    private Integer         numberOfElements;
    @NonNull
    @NotNull
    private Long            totalElements;
    @NonNull
    @NotNull
    private Integer         size;
    @NonNull
    @NotNull
    private List<LevelView> lvlViews;

    /**
     * Constructs a response with the given error set.
     *
     * @param errors set of error strings
     */
    public LevelViewPageResponse(Set<String> errors)
    {
        super(errors);
        this.totalPages = -1;
        this.number = -1;
        this.numberOfElements = -1;
        this.totalElements = -1L;
        this.size = -1;
        this.lvlViews = Collections.emptyList();
    }

    public LevelViewPageResponse(
            int totalPages,
            int number,
            int numberOfElements,
            long totalElements,
            int size,
            List<LevelView> lvlViews)
    {
        super(Collections.emptySet());
        this.totalPages = totalPages;
        this.number = number;
        this.numberOfElements = numberOfElements;
        this.totalElements = totalElements;
        this.size = size;
        this.lvlViews = lvlViews;
    }
}
