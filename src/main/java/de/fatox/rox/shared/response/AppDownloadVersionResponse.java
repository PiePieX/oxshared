package de.fatox.rox.shared.response;


import de.fatox.rox.shared.util.OxVersion;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Set;

/**
 * REST response for application version download.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@SuppressWarnings("unused")
public class AppDownloadVersionResponse extends AbstractResponse
{
    @NonNull
    @NotNull
    @Valid
    private OxVersion version;

    @NonNull
    @NotNull
    private byte[] application;

    /**
     * Constructs a response with the given error set.
     *
     * @param errors set of error strings
     */
    public AppDownloadVersionResponse(Set<String> errors)
    {
        super(errors);
        this.version = new OxVersion();
        this.application = new byte[0];
    }

    public AppDownloadVersionResponse(OxVersion version, byte[] application)
    {
        super(Collections.emptySet());
        this.version = version;
        this.application = application;
    }
}
