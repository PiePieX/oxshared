package de.fatox.rox.shared.response;


import de.fatox.rox.shared.util.ChangelogStringList;
import de.fatox.rox.shared.util.OxVersion;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Set;

/**
 * REST response for application version check.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@SuppressWarnings("unused")
public class AppCheckVersionResponse extends AbstractResponse
{
    @NotNull
    private boolean updateAvailable;

    @NonNull
    @NotNull
    @Valid
    private OxVersion currentVersion;

    @NonNull
    @NotNull
    @Valid
    private ChangelogStringList changelog;

    /**
     * Constructs a response with the given error set.
     *
     * @param errors set of error strings
     */
    public AppCheckVersionResponse(Set<String> errors)
    {
        super(errors);
        this.updateAvailable = false;
        this.currentVersion = new OxVersion();
        this.changelog = new ChangelogStringList(Collections.emptyList());
    }

    public AppCheckVersionResponse(boolean updateAvailable, OxVersion currentVersion, ChangelogStringList changelog)
    {
        super(Collections.emptySet());
        this.updateAvailable = updateAvailable;
        this.currentVersion = currentVersion;
        this.changelog = changelog;
    }
}
