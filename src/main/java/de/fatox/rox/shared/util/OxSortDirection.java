package de.fatox.rox.shared.util;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
public class OxSortDirection
{
    @NonNull
    @NotNull
    @Size(min = 3, max = 4)
    @Pattern(regexp = OxPattern.SORT_DIRECTION)
    private String direction;
}
