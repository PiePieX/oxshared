package de.fatox.rox.shared.util;


import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * A domain representation of a level used by the ROX game.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class LevelView
{
    @NonNull
    @NotNull
    private Long id;

    @NonNull
    @NotNull
    private Long version;

    @NonNull
    @NotNull
    private String author;

    @NonNull
    @NotNull
    private String name;

    @NonNull
    @NotNull
    private LocalDateTime creationDate;

    @NonNull
    @NotNull
    private LocalDateTime latestUpdateDate;
}
