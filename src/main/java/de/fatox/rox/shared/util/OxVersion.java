package de.fatox.rox.shared.util;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.Min;

/**
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@EqualsAndHashCode
public class OxVersion
{
    @NonNull
    @Min(0L)
    private Integer major;

    @NonNull
    @Min(0L)
    private Integer minor;

    @NonNull
    @Min(0L)
    private Integer build;

    public OxVersion()
    {
        this.major = -1;
        this.minor = -1;
        this.build = -1;
    }

    public OxVersion(int major, int minor, int build)
    {
        this.major = major;
        this.minor = minor;
        this.build = build;
    }

    @Override
    public String toString()
    {
        return major + "." + minor + "." + build;
    }

}
