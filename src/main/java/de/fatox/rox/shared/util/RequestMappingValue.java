package de.fatox.rox.shared.util;

/**
 * @author pinkie.swirl@mailbox.org
 */
@SuppressWarnings("unused")
public enum RequestMappingValue
{
    ;

    private static final String API = "/api";

    private static final String API_USER                   = API + "/user";
    public static final  String API_USER_REGISTER          = API_USER + "/register";
    public static final  String API_USER_CHECK_NAME        = API_USER + "/check/name";
    public static final  String API_USER_UPDATE            = API_USER + "/update";
    public static final  String API_USER_CHECK_CREDENTIALS = API_USER + "/check/credentials";
    public static final  String API_USER_DELETE            = API_USER + "/delete";

    private static final String API_LVL          = API + "/lvl";
    public static final  String API_LVL_REGISTER = API_LVL + "/register";
    public static final  String API_LVL_UPLOAD   = API_LVL + "/upload";
    public static final  String API_LVL_DOWNLOAD = API_LVL + "/download";
    public static final  String API_LVL_VIEW     = API_LVL + "/view";

    private static final String API_APP               = API + "/app";
    public static final  String API_APP_CHECK_VERSION = API_APP + "/check/version";
    public static final  String API_APP_UPLOAD        = API_APP + "/upload";
    public static final  String API_APP_DOWNLOAD      = API_APP + "/download";


    private static final String WEB = "";

    private static final String WEB_USER          = WEB + "/user";
    public static final  String WEB_USER_REGISTER = WEB_USER + "/register";
    public static final  String WEB_USER_LOGIN    = WEB_USER + "/login";
    public static final  String WEB_USER_PROFILE  = WEB_USER + "/profile";
    public static final  String WEB_USER_SETTINGS = WEB_USER + "/settings";
}
