package de.fatox.rox.shared.util;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
public class OxPassword
{
    @NonNull
    @NotNull
    @Size(min = 8, max = 64)
    @Pattern(regexp = OxPattern.PASSWORD)
    private String token;
}
