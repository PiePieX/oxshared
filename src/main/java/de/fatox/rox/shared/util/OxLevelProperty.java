package de.fatox.rox.shared.util;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
public class OxLevelProperty
{
    @NonNull
    @NotNull
    @Size(min = 4, max = 16)
    @Pattern(regexp = OxPattern.LEVEL_PROPERTY)
    private String property;
}
