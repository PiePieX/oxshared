package de.fatox.rox.shared.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.regex.Pattern;

/**
 * Helper class for {@link java.util.regex.Pattern}.
 * <p>
 * This is a central storage for pattern that can be used everywhere in the application.
 *
 * @author pinkie.swirl@mailbox.org
 */
@SuppressWarnings("unused")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum OxPattern
{
    ;

    /**
     * Pattern for names, like user name or level name.
     */
    public static final String NAME           = "[a-zA-Z0-9\\._\\-]{3,35}";
    public static final Pattern NAME_COMPILED           = Pattern.compile(NAME);
    /**
     * Pattern for a line in a changelog.
     */
    public static final String CHANGELOG_LINE = "[a-zA-Z0-9\\._\\-\\?\\!\\s]{3,1000}";
    //public static final Pattern CHANGELOG_LINE_COMPILED = Pattern.compile(CHANGELOG_LINE);
    /**
     * Pattern for the rox application version.
     */
    public static final String APP_VERSION    = "(([1-9][0-9]*)|([0]))\\.(([1-9][0-9]*)|([0]))\\.(([1-9][0-9]*)|([0]))";
    //public static final Pattern APP_VERSION_COMPILED    = Pattern.compile(APP_VERSION);
    /**
     * Pattern for page sorting.
     */
    public static final String SORT_DIRECTION = "(?:ASC|DESC)";
    //public static final Pattern SORT_DIRECTION_COMPILED = Pattern.compile(SORT_DIRECTION);
    /**
     * Pattern for domain entity properties (field names).
     */
    public static final String LEVEL_PROPERTY = "(?:author.name|latestUpdateDate|version|name)";
    //public static final Pattern LEVEL_PROPERTY_COMPILED = Pattern.compile(LEVEL_PROPERTY);
    /**
     * Pattern for password.
     */
    public static final String PASSWORD       = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%]).{8,64})";
    public static final Pattern PASSWORD_COMPILED       = Pattern.compile(PASSWORD);
}
