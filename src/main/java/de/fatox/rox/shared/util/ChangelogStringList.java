package de.fatox.rox.shared.util;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
public class ChangelogStringList
{
    @NonNull
    @NotNull
    @Size(min = 1)
    private List<ChangelogString> lines;
}
