package de.fatox.rox.shared.util;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author pinkie.swirl@mailbox.org
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
public class OxName
{
    @NonNull
    @NotNull
    @Size(min = 3, max = 35, message = "{oxUser.register.invalid.length}")
    @Pattern(regexp = OxPattern.NAME, message = "{oxUser.register.invalid.chars}")
    private String name;
}
