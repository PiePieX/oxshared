package de.fatox.rox.shared.request;


import de.fatox.rox.shared.util.OxName;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * REST request for level upload.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Data
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
@SuppressWarnings("unused")
public class LevelUploadRequest
{
    @NonNull
    @Min(0L)
    @Max(Long.MAX_VALUE)
    private Long id;

    @NonNull
    @NotNull
    @Valid
    private OxName name;

    @NonNull
    @NotNull
    private String data;
}
