package de.fatox.rox.shared.request;


import de.fatox.rox.shared.util.OxName;
import de.fatox.rox.shared.util.OxPassword;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * REST request for existing user updating.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Data
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
@SuppressWarnings("unused")
public class OxUserUpdateRequest
{
    @NonNull
    @NotNull
    @Valid
    private OxName name;

    @NonNull
    @NotNull
    @Valid
    private OxPassword token;

    @NonNull
    @NotNull
    private OxPassword tokenConfirm;
}
