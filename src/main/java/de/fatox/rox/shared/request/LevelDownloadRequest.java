package de.fatox.rox.shared.request;


import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * REST request for level download.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Data
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
@SuppressWarnings("unused")
public class LevelDownloadRequest
{
    /**
     * The level ID to download.
     */
    @NonNull
    @NotNull
    @Min(0L)
    @Max(Long.MAX_VALUE)
    private Long id;
}
