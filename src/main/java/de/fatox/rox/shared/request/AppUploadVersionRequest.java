package de.fatox.rox.shared.request;


import de.fatox.rox.shared.util.ChangelogStringList;
import de.fatox.rox.shared.util.OxVersion;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * REST request for uploading a new application version.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Data
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
@SuppressWarnings("unused")
public class AppUploadVersionRequest
{
    /**
     * The application version to upload.
     */
    @NonNull
    @NotNull
    @Valid
    private OxVersion version;

    /**
     * The changelog lines.
     */
    @NonNull
    @NotNull
    @Valid
    private ChangelogStringList changelog;

    /**
     * The application data in bytes.
     */
    @NonNull
    @NotNull
    private byte[] application;
}
