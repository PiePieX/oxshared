package de.fatox.rox.shared.request;


import de.fatox.rox.shared.util.OxVersion;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * REST request for application version download.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Data
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
@SuppressWarnings("unused")
public class AppDownloadVersionRequest
{
    /**
     * The application version to download.
     */
    @NonNull
    @NotNull
    @Valid
    private OxVersion version;
}
