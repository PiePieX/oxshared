package de.fatox.rox.shared.request;


import de.fatox.rox.shared.util.OxLevelProperty;
import de.fatox.rox.shared.util.OxSortDirection;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * REST request for level download.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Data
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
@SuppressWarnings("unused")
public class LevelViewPageRequest
{
    @NonNull
    @NotNull
    @Min(0L)
    @Max(Integer.MAX_VALUE)
    private Integer page;

    @NonNull
    @NotNull
    @Min(1L)
    @Max(10L)
    private Integer size;

    @NonNull
    @NotNull
    @Valid
    private OxSortDirection direction;

    @NonNull
    @NotNull
    @Valid
    private OxLevelProperty property;
}
