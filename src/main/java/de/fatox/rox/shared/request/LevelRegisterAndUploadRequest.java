package de.fatox.rox.shared.request;


import de.fatox.rox.shared.util.OxName;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * REST request for level register and upload.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Data
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
@SuppressWarnings("unused")
public class LevelRegisterAndUploadRequest
{
    @NonNull
    @NotNull
    @Valid
    private OxName name;

    @NonNull
    @NotNull
    private String data;
}
