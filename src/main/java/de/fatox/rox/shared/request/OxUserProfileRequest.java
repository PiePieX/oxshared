package de.fatox.rox.shared.request;


import de.fatox.rox.shared.util.OxName;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * REST request for user profile viewing.
 *
 * @author pinkie.swirl@mailbox.org
 */
@Data
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
@SuppressWarnings("unused")
public class OxUserProfileRequest
{
    @NonNull
    @NotNull
    @Valid
    private OxName name;
}
